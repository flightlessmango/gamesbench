FROM archlinux:latest
ARG STEAMPASSW
ARG STEAMUSER
RUN pacman-key --init && \
pacman-key --populate archlinux && \
pacman-key --recv-key 3056513887B78AEB --keyserver keyserver.ubuntu.com && \
pacman-key --lsign-key 3056513887B78AEB
RUN echo "ParallelDownloads = 10" >> /etc/pacman.conf
# RUN echo "\n" && echo "[chaotic-aur]" >> /etc/pacman.conf && echo "Include = /etc/pacman.d/chaotic-mirrorlist" >> /etc/pacman.conf && \
RUN echo "\n" && echo "[multilib]" >> /etc/pacman.conf && echo "Include = /etc/pacman.d/mirrorlist" >> /etc/pacman.conf
RUN pacman -Sy archlinux-keyring --noconfirm
RUN pacman -S base-devel steam meson python-mako \
glslang libglvnd lib32-libglvnd libxnvctrl git wget \
python-pip lib32-sdl lib32-sdl2 libffi xorg-server-xvfb \
xorg-server python-pydantic python-yaml python-pillow \
python-pytesseract xdotool tesseract-data-eng xorg-xinit \
xorg-xhost xterm vulkan-radeon lib32-vulkan-radeon --noconfirm
RUN pip install python-uinput pynput python-libxdo vdf --no-input
RUN rm -r /etc/X11/xinit/xinitrc && \
echo "xhost +" >> /etc/X11/xinit/xinitrc && \
echo "xterm" >> /etc/X11/xinit/xinitrc && \
echo "allowed_users = anybody" >> /etc/X11/Xwrapper.config && \
echo "needs_root_rights=yes" >> /etc/X11/Xwrapper.config
RUN useradd steamuser && mkdir /home/steamuser && \
chown steamuser:steamuser /home/steamuser && \ 
echo "steamuser ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
RUN sudo usermod -a -G kmem,tty,wheel,video steamuser
USER steamuser
WORKDIR /home/steamuser
RUN mkdir gamesbench
COPY steam_install.py /home/steamuser/gamesbench/
RUN git clone https://aur.archlinux.org/steamcmd.git && \
cd steamcmd && \
makepkg -si --noconfirm && \
rm -r ../steamcmd
RUN python gamesbench/steam_install.py first_run
COPY install_proton.sh .
RUN ./install_proton.sh
RUN wget https://github.com/flightlessmango/MangoHud/releases/download/v0.6.8/MangoHud-0.6.8.r0.gefdcc6d.tar.gz && \
tar zxvf MangoHud-0.6.8.r0.gefdcc6d.tar.gz && cd MangoHud && \
./mangohud-setup.sh install && cd && rm -r MangoHud
RUN git clone https://github.com/flightlessmango/MangoHud && \
cd MangoHud/control/ && sudo python setup.py install
COPY steam.py parse.py control.py utils.py runner.py client.py /home/steamuser/gamesbench/
