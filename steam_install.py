import sys
import os
import subprocess
import yaml

def steam_first_run():
    password = os.environ.get("STEAMPASSW", None)
    username = os.environ.get("STEAMUSER", None)
    if password and username:
        xvfb = subprocess.Popen('Xvfb :2', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print("Running Steam for the first time", file=sys.stderr)
        steam = subprocess.Popen('DISPLAY=:2 steam -no-cef-sandbox -login ' + username + " " + password , shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        for line in steam.stderr:
            if "RegisterForAppOverview" in str(line.strip()):
                print("Killing steam", file=sys.stderr)
                subprocess.run("pkill -9 -f steam",
                                shell=True, stdout=subprocess.DEVNULL)
                subprocess.run("pkill -9 -f Xvfb",
                                shell=True, stdout=subprocess.DEVNULL)
    else:
        print("Missing steam username or password", file=sys.stderr)
        exit(1)

def install_game(appid=None, proton=False):
    print("installing game", file=sys.stderr)
    password = os.environ.get("STEAMPASSW"  , None)
    username = os.environ.get("STEAMUSER"   , None)
    appid    = os.environ.get("APPID"       , None)

    if os.path.isfile("game.yml"):
        print("checking game.yml for proton and appid", file=sys.stderr)
        data = yaml.safe_load(open("game.yml"))
        proton = data["game_bench_automation"]["game"]["steam"]["proton"]
        appid = data["game_bench_automation"]["game"]["steam"]["app_id"]

    if appid and username and password:
        platform = "windows" if proton else "linux"
        print("platform:", platform, file=sys.stderr)
        steamcmd = subprocess.Popen(["steamcmd", "+@sSteamCmdForcePlatformType", platform,
                                    "+login", username, password, "+app_update", str(appid), "validate +quit"],
                                    stdout=subprocess.PIPE)
        for line in steamcmd.stdout:
            if ("Invalid Password" in str(line) or
                "Invalid platform" in str(line) or
                "No subscription" in str(line)):
                print(line.strip(), file=sys.stderr)
        exit(steamcmd.wait())
    else:
        print("Missing steam username, password or appid.", file=sys.stderr)
        exit(1)

if __name__ == "__main__":
    if sys.argv[1] == "first_run":
        steam_first_run()

    if sys.argv[1] == "install_game":
        install_game()
