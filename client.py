import requests
import os
import sys
import base64
import hashlib
from requests.packages.urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter

class Client:
    def __init__(self, url, username=None):
        self.url = url
        self.username = username
        
        self._login_cookie = None

    def login(self):
        if self._login_cookie is None:
            password = os.environ.get("VALVETRACES_PASSWORD", None)
            if password is None:
                password = os.environ.get("VALVETRACESPASSWORD", None)
            if self.username is None or password is None:
                print("ERROR: credentials not specified for valve traces client")
                sys.exit(1)

            r = requests.post(f"{self.url}/api/v1/login", allow_redirects=False,
                              json={"user": {"username": self.username, "password": password}})
            r.raise_for_status()

            self._login_cookie = r.cookies

        return self._login_cookie
    
    def _data_checksum(self, filepath):
        hash_md5 = hashlib.md5()
        with open(filepath, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)

            return base64.b64encode(hash_md5.digest()).decode()

    def _upload_blob(self, filepath, name, data_checksum, image_checksum=None):
        with open(filepath, "rb") as f:
            # Check the file size
            f.seek(0, os.SEEK_END)
            file_size = f.tell()
            f.seek(0, os.SEEK_SET)

            # Ask the website for the URL of where to upload the file
            r_blob = self._post("/rails/active_storage/direct_uploads",
                                {"blob": {"filename": name, "byte_size": file_size,
                                          "checksum": self._data_checksum(filepath),
                                          "image_checksum": image_checksum}})
            blob = Blob(r_blob)

            # Send the file to the bucket
            blob.upload(f)
            return blob

    @property
    def requests_retry_session(self, retries=3, backoff_factor=0.3, status_forcelist=(500, 502, 504), session=None):
        session = session or requests.Session()
        retry = Retry(
            total=retries,
            read=retries,
            connect=retries,
            backoff_factor=backoff_factor,
            status_forcelist=status_forcelist,
        )
        adapter = HTTPAdapter(max_retries=retry)
        session.mount('http://', adapter)
        session.mount('https://', adapter)
        return session

    def _post(self, path, params):
        err_msg = f"ERROR while executing the POST query to {self.url}{path} with the following parameters: {params}"
        try:
            r = self.requests_retry_session.post(f"{self.url}{path}",
                                                 allow_redirects=False,
                                                 cookies=self.login(),
                                                 json=params)
        except Exception as e:
            print(err_msg)
            raise e

        if r.status_code == 500:
            print(f"{err_msg}\nReturn value: {r.text}")
        elif r.status_code == 409:
            # TODO: Add an option to ignore some errors
            return r.json()

        r.raise_for_status()

        return r.json()

class Blob:
    def __init__(self, blob_dict, new=True):
        self.new = new
        direct_upload = blob_dict.get("direct_upload")
        if direct_upload is not None:
            self.url = direct_upload.get('url')
            if self.url is None:
                raise ValueError("The URL is missing from the 'direct_upload' dict")

            self.headers = direct_upload.get('headers')
            if self.headers is None:
                raise ValueError("The headers are missing from the 'direct_upload' dict")

        self.signed_id = blob_dict.get("signed_id")
        if self.signed_id is None:
            raise ValueError("The signed_id is missing from the blob-creation response")

    def upload(self, f):
        r = requests.put(self.url, headers=self.headers, data=f)
        r.raise_for_status()