import pydantic
import yaml
import re
import datetime
import time
import os
from shutil import copy
from time import sleep
from utils import StateEnterOnConditions
from pydantic.dataclasses import dataclass
from dataclasses import asdict, field, fields
from enum import Enum, IntEnum
from pydantic import root_validator, validator
#TODO: rename control to controller and realController to driver
from control import Controller as ControllerDriver
from control import Replayer
from pathlib import Path

def time_str_to_timedelta(v):
    regex = re.compile(r'^((?P<hours>[\.\d]+?)h)?((?P<minutes>[\.\d]+?)m)?((?P<seconds>[\.\d]+?)s)?((?P<milliseconds>[\.\d]+?)ms)?$')
    parts = regex.match(v)
    assert parts is not None, "Could not parse any time information from '{}'.  Examples of valid strings: '8h', '2d8h5m20s', '2m4s'".format(v)
    time_params = {name: float(param) for name, param in parts.groupdict().items() if param}
    return datetime.timedelta(**time_params)

class HierarchyMixIn:
    def set_hierarchy(self, root, parent=None):
        def recursive_set(field):
            if isinstance(field, HierarchyMixIn):
                field.set_hierarchy(root, self)
            elif isinstance(field, list):
                for item in field:
                    recursive_set(item)
            elif isinstance(field, dict):
                for item in field.values():
                    recursive_set(item)
            # Ignore all other types

        self.root = root
        self.parent = parent

        # Recursively set the fields
        for field in fields(self):
            if field_value := getattr(self, field.name):
                recursive_set(field_value)

@dataclass
class GameSteam(HierarchyMixIn):
    app_id: int
    executable_name: str
    launch_options: str = None
    proton: bool = None

@dataclass
class Game(HierarchyMixIn):
    steam: GameSteam
    environment: dict[str, str] = field(default_factory=dict)
    save_location: str = None
    saves: list[pydantic.FilePath] = None
    setting_location: str = None
    settings: list[pydantic.FilePath] = None
    
    def __post_init_post_parse__(self):
        if self.save_location:
            self.save_location = str(Path.home()) + self.save_location
            Path(self.save_location).mkdir(parents=True, exist_ok=True)
            for save in self.saves:
                copy(save, self.save_location)

        if self.setting_location:            
            self.setting_location = str(Path.home()) + self.setting_location
            Path(self.setting_location).mkdir(parents=True, exist_ok=True)
            for setting in self.settings:
                copy(setting, self.setting_location)
                
        env = dict(os.environ)
        for key, val in self.environment.items():
            env[key] = val
        
        self.environment = os.environ

class ControllerModel(str, Enum):
    XBOX_360 = "XBOX 360"

@dataclass
class Controller(HierarchyMixIn):
    model: ControllerModel

    def __post_init_post_parse__(self):
        # os.system("sudo modprobe uinput")
        #TODO: check that uinput is loaded
        self.driver = ControllerDriver()

@dataclass
class StateEnterOnConditionOCR(HierarchyMixIn):
    bounding_box: str = None
    regex: str = None
    
    @validator('bounding_box')
    def bounding_box_string_to_tuple(cls, v):
        if v:
            return tuple(map(int, v.split(",")))

@dataclass
class StateEnterOnConditionColorCheck(HierarchyMixIn):
    location: str
    color: str
    
    @validator('location')
    def location_string_to_tuple(cls, v):
        return tuple(map(int, v.split(",")))

@dataclass
class StateEnterOnCondition(HierarchyMixIn):
    OCR: StateEnterOnConditionOCR = None
    color_check: StateEnterOnConditionColorCheck = None
    
    def checkCondition(cls):
        if cls.OCR:
            if cls.OCR.bounding_box:
                return StateEnterOnConditions.find_sentence(cls.OCR.regex, cls.OCR.bounding_box)
            else:
                return StateEnterOnConditions.find_sentence_fullscreen(cls.OCR.regex)
        elif cls.color_check:
            return StateEnterOnConditions.check_pixel(cls.color_check.color, cls.color_check.location)

@dataclass
class ActionControllerPress(HierarchyMixIn):
    buttons: list[str]
    duration: str = "250ms"

    @validator('duration')
    def duration_string_to_timedelta(cls, v):
        return time_str_to_timedelta(v)

@dataclass
class ActionControllerTrace(HierarchyMixIn):
    file: pydantic.FilePath

@dataclass
class ActionController(HierarchyMixIn):
    press: ActionControllerPress = None
    trace: ActionControllerTrace = None
    
    @root_validator()
    def check_that_only_one_action_is_set(cls, values):
        has_action = False
        for field in fields(cls):
            if values.get(field.name) is not None:
                if has_action:
                    raise ValueError("Cannot have more than one action per ActionController object")
                else:
                    has_action = True
                
        return values
    
    def perform(self, controller_name):
        controller = self.root.controllers[controller_name]
        if self.press:
            for btn in self.press.buttons:
                controller.driver.emit(btn, 1)
                sleep(self.press.duration.total_seconds())
                controller.driver.emit(btn, 0)

        if self.trace:
            Replayer(controller.driver, self.trace.file)

class ActionBenchmarkAction(str, Enum):
    START = "start"
    STOP = "stop"

@dataclass
class ActionBenchmark(HierarchyMixIn):
    action: ActionBenchmarkAction
    name: str = None
    metrics: list[str] = None

class ActionExitStatus(str, Enum):
    SUCCESS = "success"
    FAILURE = "failure"
    TIMEOUT = "timeout"

@dataclass
class ActionExit(HierarchyMixIn):
    status: ActionExitStatus
    
    def perform(self):
        for i, status in enumerate(ActionExitStatus):
            if status == self.status:
                print(f"Exiting with status {i} ({self.status.name})")
                exit(i)

@dataclass
class Action(HierarchyMixIn):
    benchmark: ActionBenchmark = None
    controllers: dict[str, ActionController] = None
    sleep: str = None
    exit: ActionExit = None

    @root_validator()
    def check_that_only_one_action_is_set(cls, values):
        has_action = False
        for field in fields(cls):
            if values.get(field.name) is not None:
                if has_action:
                    raise ValueError("Cannot have more than one action per Action object")
                else:
                    has_action = True
                
        return values
    
    @validator('sleep')
    def sleep_string_to_timedelta(cls, v):
        if v:
            return time_str_to_timedelta(v)
        else: 
            return None

    def perform(self):
        if self.benchmark:
            self.benchmark.perform()
        elif self.controllers:
            for controller_name, controller_action in self.controllers.items():
                controller_action.perform(controller_name)
        elif self.sleep:
            time.sleep(self.sleep.total_seconds())
        elif self.exit:
            self.exit.perform()

@dataclass
class State(HierarchyMixIn):
    name: str
    enter_on: dict[str, list[StateEnterOnCondition]] = field(default_factory=dict)
    actions: list[Action] = field(default_factory=list)
    timeout: str = "30s"
    
    @validator('timeout')
    def timeout_string_to_timedelta(cls, v):
        return time_str_to_timedelta(v)
                
    def checkState(self):
        for conditions in self.enter_on:
            for condition in self.enter_on[conditions]:
            #TODO: Only return true if all the sub conditions are met
                if condition.checkCondition():
                    return True
            
        return False
          
    def runActions(self):
        for action in self.actions:
            action.perform()

@dataclass
class GameBenchAutomation(HierarchyMixIn):
    version: int
    name: str
    game: Game
    states: list[State] = field(default_factory=list)
    controllers: dict[str, Controller] = field(default_factory=dict)

    def __post_init_post_parse__(self):
        for name, controller in self.controllers.items():
            controller.name = name

        self.set_hierarchy(root=self)

    def run(self):
        for i, state in enumerate(self.states):
            go_to_next_state = False
            start_time = time.time()
            while not go_to_next_state:
                if state.checkState():
                    state.runActions()
                    if self.states[-1] is state:
                        return True

                # check if we've reached the next state
                if self.states[-1] is not state:
                    go_to_next_state = self.states[i + 1].checkState()

                # Go to next state if we time out
                if (time.time() - start_time) > state.timeout.seconds:
                    print("State: [" + state.name + "] timed out after " + str(state.timeout.seconds) + " second(s)")
                    go_to_next_state = True
    
    # Expose directly properties from self.game.steam
    def __getattr__(self, attr):
        return getattr(self.game.steam, attr)

@dataclass
class BenchAutomation:
    game_bench_automation: GameBenchAutomation
    
    @classmethod
    def from_file(cls, file_path):
        with open(file_path, 'r') as f:
            data = yaml.safe_load(f)
            return cls(**data if data else {})