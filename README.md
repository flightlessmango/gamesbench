# Introduction
This project attemps to automate game benchmarking

# Usage
#### Base container
- The base container is an arch base with steam, mesa, mangohud etc installed.
- The docker build will login to steam and proceed to download proton-experimental, soldier runtime and steamworks redist.
- The Steam account has to have steamguard disabled for the container to automate the login
- `docker build . --build-arg STEAMUSER=$steam_username --build-arg STEAMPASSWD=$steam_password`


#### Running without a container
##### warning: this is potentially destructive.
- `cd` into the preferred games folder in this project
- run `python ../../runner.py`