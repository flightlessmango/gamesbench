import sys
import os
import vdf
import threading
import subprocess
from time import sleep
from pathlib import Path
from control import Controller
from xdo import Xdo

class Steam:
    def __init__(self, appid, launch_options, proton, env):
        self.set_offline()
        self.disable_steam_overlay()
        self.disable_cloud_saves()
        if proton:
            self.set_steam_play(str(appid))
        self.set_launch_options(appid, launch_options, proton)
        # self.xorg = threading.Thread(target=self.xorg_thread) 
        # self.xorg.start()
        self.steam = threading.Thread(target=self.steam_thread, args=(str(appid), env,))
        self.steam.start()
        
    def is_steam_dialog(self):
        xdo = Xdo()
        title = xdo.get_window_name(xdo.get_active_window())
        if "Steam Dialog" in str(title):
            print("Steam dialog found")
            ctrl = Controller()
            ctrl.sendkeys("Tab")
            ctrl.sendkeys("space")
        
    def xorg_thread(self):
        print("starting X")
        os.system("startx -- :1")

    def steam_thread(self, appid, env):
        env["DISPLAY"] = ":0"
        steam = subprocess.Popen(["steam", "-no-cef-sandbox", "-applaunch", appid],
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=env)

        for line in steam.stderr:
            if "syncfailed" in str(line):
                print("Sync failed")
        
        steam.wait()

    def is_alive(self):
        return self.steam.is_alive()
    
    def is_mangohud_attached(self):
            lsof32 = subprocess.Popen(["lsof", "/usr/lib/mangohud/lib32/libMangoHud.so"],
                                       stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            lsof64 = subprocess.Popen(["lsof", "/usr/lib/mangohud/lib64/libMangoHud.so"],
                                       stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            return lsof32.wait() == 0 or lsof64.wait() == 0

    def is_game_running(self, exe_name):
        pgrep = subprocess.Popen(["pgrep", str(exe_name)], stdout=subprocess.DEVNULL)
        return pgrep.wait() == 0
    
    def kill_game(self, exe_name):
        kill = subprocess.Popen(["sudo", "pkill", "-15", "-f", str(exe_name)], stdout=subprocess.DEVNULL)
        return kill.wait() == 0
    
    def set_steam_play(self, appid):
        print("setting steam play")
        path = str(Path.home()) + "/.steam/steam/config/config.vdf"
        file = open(path)
        loaded = vdf.VDFDict(vdf.load(file))
        if not "CompatToolMapping" in loaded["InstallConfigStore"]["Software"]["Valve"]["Steam"].keys():
            loaded["InstallConfigStore"]["Software"]["Valve"]["Steam"]["CompatToolMapping"] = {}

        loaded["InstallConfigStore"]["Software"]["Valve"]["Steam"]["CompatToolMapping"][appid] = {}
        loaded["InstallConfigStore"]["Software"]["Valve"]["Steam"]["CompatToolMapping"][appid]["name"] = "proton_experimental"
        loaded["InstallConfigStore"]["Software"]["Valve"]["Steam"]["CompatToolMapping"][appid]["config"] = ""
        loaded["InstallConfigStore"]["Software"]["Valve"]["Steam"]["CompatToolMapping"][appid]["priority"] = "250"
        vdf.dump(loaded, open(path, "w"), pretty=True)

    def set_offline(self):
        path = str(Path.home()) + "/.steam/steam/config/loginusers.vdf"
        file = open(path)
        loaded = vdf.VDFDict(vdf.load(file))
        print("setting offline")
        for user in loaded["users"]:
            user = loaded["users"][user]
            user["RememberPassword"] = 1
            user["AllowAutoLogin"] = 1
            user["MostRecent"] = 1
            user["WantsOfflineMode"] = 1
            user["SkipOfflineModeWarning"] = 1
            user["Timestamp"] = os.popen("date +%s").read().strip()
        vdf.dump(loaded, open(path, "w"), pretty=True)

    def disable_steam_overlay(self):
        print("disabling steam overlay")
        for dir_ in os.listdir(str(Path.home()) + "/.steam/steam/userdata"):
            path = str(Path.home()) + "/.steam/steam/userdata/" + dir_ + "/config/localconfig.vdf"
            if not os.path.isfile(path):
                continue
            file = open(path)
            loaded = vdf.VDFDict(vdf.load(file))
            if not "system" in loaded["UserLocalConfigStore"]:
                loaded["UserLocalConfigStore"]["system"] = {}
            loaded["UserLocalConfigStore"]["system"]["EnableGameOverlay"] = 0
            vdf.dump(loaded, open(path, "w"), pretty=True)

    def set_launch_options(self, appid, launch_options, proton):
        print("setting launch options")
        for dir_ in os.listdir(str(Path.home()) + "/.steam/steam/userdata"):
            path = path = str(Path.home()) + "/.steam/steam/userdata/" + dir_ + "/config/localconfig.vdf"
            if not os.path.isfile(path):
                continue
            file = open(path)
            loaded = vdf.VDFDict(vdf.load(file))
            if not "1236990" in loaded["UserLocalConfigStore"]["Software"]["Valve"]["Steam"]["apps"]:
                loaded["UserLocalConfigStore"]["Software"]["Valve"]["Steam"]["apps"][str(appid)] = {}
            app = loaded["UserLocalConfigStore"]["Software"]["Valve"]["Steam"]["apps"][str(appid)]
            app["LastPlayed"] = "1616072468"
            app["Playtime"] = "1"
            app[str(appid) + "_eula_0"] = "0"
            app["LaunchOptions"] = launch_options
            if not "cloud" in app:
                app["cloud"] = {}
            app["cloud"]["last_sync_state"] = "synchronized"
            if not "autocloud" in app:
                app["autocloud"] = {}
            app["autocloud"]["lastlaunch"] = "1659586376"
            app["autocloud"]["lastexit"] = "1659586401"
            if proton:
                app["ViewedSteamPlay"] = 1
            vdf.dump(loaded, open(path, "w"), pretty=True)
            
    def disable_cloud_saves(self):
        print("disabling cloud saves")
        for dir_ in os.listdir(str(Path.home()) + "/.steam/steam/userdata"):
            path = str(Path.home()) + "/.steam/steam/userdata/" + dir_ + "/7/remote/sharedconfig.vdf"
            if not os.path.isfile(path):
                continue
            file = open(path)
            loaded = vdf.VDFDict(vdf.load(file))
            loaded["UserRoamingConfigStore"]["Software"]["Valve"]["Steam"]["CloudEnabled"] = 0
            vdf.dump(loaded, open(path, "w"), pretty=True)