from steam import Steam
from parse import BenchAutomation

def main():
	automation = BenchAutomation.from_file("witcher.yml").game_bench_automation
	benchmark_ready = False
	steam = Steam(automation.appid(), automation.launchOptions())
	while steam.is_alive():
		print("steam is running")
		while steam.game_running(automation.executableName()):
			print("game is running")
			if not benchmark_ready:
				print("starting automation")
				benchmark_ready = automation.run()
				print("benchmark done")
main()
