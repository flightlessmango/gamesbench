from steam import Steam
from parse import BenchAutomation
from client import Client
import os
from pathlib import Path
import time
from utils import StateEnterOnConditions as util
from time import sleep

class Runner():
    def __init__(self):
        self.main()

    def main(self, automation=None, benchmark_ready=False):
        automation = BenchAutomation.from_file("game.yml").game_bench_automation
        steam = Steam(automation.app_id, automation.launch_options,
                      automation.proton, automation.game.environment)
        while steam.is_alive():
            while not steam.is_game_running(automation.executable_name):
                steam.is_steam_dialog()
            if steam.is_game_running(automation.executable_name):
                if not benchmark_ready:
                    print("starting automation")
                    benchmark_ready = automation.run()
        
    def upload_log(self, client, log_file=None):
        # find log
        for file in os.listdir(Path.home()):
            if ".csv" in file and not "summary" in file:
                log_file = str(Path.home()) + "/" + file
        checksum = client._data_checksum(log_file)
        blob = client._upload_blob(log_file, "test run", checksum)
        r = client._post("/api/v1/runs", {"run": {"game_id": 2, "name": "test run",
                              "upload": blob.signed_id}})

Runner()