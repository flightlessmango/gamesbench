from PIL import Image
from PIL import ImageGrab
import pytesseract
import re
from control import Controller
from time import sleep

class StateEnterOnConditions:
    @classmethod
    def find_sentence(cls, regex, geom):
        img = ImageGrab.grab(bbox=geom).convert("L")
        tess = pytesseract.image_to_string(img, config="--psm 11").strip()
        x = re.search(regex, tess)
        print(tess)
        return x

    @classmethod
    def find_sentence_fullscreen(cls, regex):
        img = ImageGrab.grab((0, 0, 1920, 1080)).convert("L")
        tess = pytesseract.image_to_string(img, config="--psm 11").strip()
        x = re.search(regex, tess)
        return x
    
    @classmethod
    def find_sentence_one(cls, regex):
        tess = pytesseract.image_to_string(Image.open("intro.png"), config="--psm 11").strip()
        x = re.search(regex, tess)
        return x

    @classmethod
    def bottom_right(cls, color):
        img = ImageGrab.grab(bbox=(1919,1079,1920,1080))
        data = img.getdata()
        return color == data[-1]

    @classmethod
    def check_pixel(cls, color, location):
        location = location + (1920, 1080)
        img = ImageGrab.grab(bbox=location)
        data = img.getdata()
        color = color.lstrip("#")
        color = tuple(int(color[i:i+2], 16) for i in (0, 2, 4))
        return color == data[0]