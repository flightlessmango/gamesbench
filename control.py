import time
from time import sleep
import subprocess
import uinput
import re
from pydantic.dataclasses import dataclass
from xdo import Xdo

class Controller:
    def __init__(self):
        self.xdo = Xdo()
        events = (
            uinput.BTN_A,
            uinput.BTN_B,
            uinput.BTN_X,
            uinput.BTN_Y,
            uinput.BTN_TL,
            uinput.BTN_TR,
            uinput.BTN_SELECT,
            uinput.BTN_START,
            uinput.BTN_THUMBL,
            uinput.BTN_THUMBR,
            uinput.ABS_X + (0, 255, 0, 0),
            uinput.ABS_Y + (0, 255, 0, 0),
            uinput.ABS_Z + (0, 255, 0, 0),
            uinput.ABS_RX + (0, 255, 0, 0),
            uinput.ABS_RY + (0, 255, 0, 0),
            uinput.ABS_RZ + (0, 255, 0, 0),
            uinput.ABS_HAT0X + (0, 255, 0, 0),
            uinput.ABS_HAT0Y + (0, 255, 0, 0),
        )

        self.device = uinput.Device(
            events,
            vendor=0x045e,
            product=0x028e,
            version=0x110,
            name="Microsoft X-Box 360 pad",
        )

        # Center joystick
        # syn=False to emit an "atomic" (128, 128) event.
        self.device.emit(uinput.ABS_X, 128, syn=False)
        self.device.emit(uinput.ABS_Y, 128)
        self.device.emit(uinput.ABS_RX, 128)
        self.device.emit(uinput.ABS_RY, 128)
        self.device.emit(uinput.ABS_HAT0X, 128, syn=False)
        self.device.emit(uinput.ABS_HAT0Y, 128)

        self.btnMap = {
            "0": uinput.BTN_A,
            "1": uinput.BTN_B,
            "2": uinput.BTN_X,
            "3": uinput.BTN_Y,
            "4": uinput.BTN_TL,
            "5": uinput.BTN_TR,
            "6": uinput.BTN_SELECT,
            "7": uinput.BTN_START,
            "9": uinput.BTN_THUMBL,
            "10": uinput.BTN_THUMBR
        }

        self.axisMap = {
            "0": uinput.ABS_X,
            "1": uinput.ABS_Y,
            "2": uinput.ABS_Z,
            "3": uinput.ABS_RX,
            "4": uinput.ABS_RY,
            "5": uinput.ABS_RZ,
            "6": uinput.ABS_HAT0X,
            "7": uinput.ABS_HAT0Y
        }
        
        self.strBtnMap = {
            "A":        uinput.BTN_A,
            "B":        uinput.BTN_B,
            "X":        uinput.BTN_X,
            "Y":        uinput.BTN_Y,
            "LT":       uinput.BTN_TL,
            "RT":       uinput.BTN_TR,
            "START":    uinput.BTN_START,
            "SELECT":   uinput.BTN_SELECT,
        }
    
    def turnjoystick(self, duration, event, amount):
        self.device.emit(event, amount)
        sleep(duration)
        # self.device.emit(event, 128)
        
    def emit(self, event, amount):
        if type(event) == str:
           self.device.emit(self.strBtnMap[event], amount)
        else:
            self.device.emit(event, amount)

    def sendkeys(self, *keys):
        for k in keys: self.xdo.send_keysequence_window(0, k.encode())

@dataclass
class ControllerEvent():
    type: int
    time: int
    number: str
    value: int
    timestamp_ms: int

class Replayer:
    def __init__(self, controller, trace_path):
        self.events = []
        self.controller = controller
        self.parse_trace(trace_path)
        self.replaying()
        self.resetController()

    def parse_trace(self, trace_path):
        trace = open(trace_path, "r")
        for i, line in enumerate(trace):
            if "type" in line:
                line = re.sub('[^0-9,-.]', '', line).split(",")
                if line[0] != "129" and line[0] != "130":
                    event = ControllerEvent(0, (int(line[1]) - 100), 0, 0, 0)
                    self.events.append(event)
                    break

        trace = open(trace_path, "r")
        for i, line in enumerate(trace):
            # Remove unneeded info and split into array
            line = re.sub('[^0-9,-.]', '', line).split(",")

            # First 4 lines is irrelevant
            if i > 4:
                # First events timestamp_ms is set to 0 for baseline
                event = ControllerEvent(*line, 0)
                if len(self.events) > 0:
                    event.timestamp_ms = int(event.time - self.events[0].time)
            else:
                continue

            # LOOK AT THIS APPEND EVEN LOGIC AGAIN WITH FRESH EYES
            # Axis values need to be converted from 16bit to 8bit
            if event.type == 2:
                event.value = int((event.value + 32767) / 255)
            
            if event.type <= 2:
                self.events.append(event)

    def cur_time_ms(self):
        return time.clock_gettime_ns(time.CLOCK_MONOTONIC_RAW) / 1e6

    def replaying(self):
        events = self.events
        ctrl = self.controller
        start_ms = self.cur_time_ms()
        cur_ms = 0
        for i, event in enumerate(events):
            if event.type == 1:
                ctrl.emit(ctrl.btnMap[event.number], event.value)
            if event.type == 2:
                ctrl.emit(ctrl.axisMap[event.number], event.value)

            next_event = events[i+1] if i+1 < len(events) else None
            if next_event:
                sleep_time = (next_event.timestamp_ms - cur_ms) / 1000.0
                if sleep_time > 0:
                    time.sleep(sleep_time)
                cur_ms = self.cur_time_ms() - start_ms

        end_ms = self.cur_time_ms()
        print("Trace ended after: ", (end_ms - start_ms) / 1000, "seconds")

    def resetController(self):
        ctrl = self.controller
        for key in ctrl.axisMap:
            ctrl.device.emit(ctrl.axisMap[key], 128)

        for key in ctrl.btnMap:
            ctrl.device.emit(ctrl.btnMap[key], 0)
